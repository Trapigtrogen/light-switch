#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <QDebug>
#include <QTimer>

#include <QMessageBox>
#include <QKeyEvent>

#include <device.h>

// Json parser
#include <nlohmann/json.hpp>
using json = nlohmann::json;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QString IP = "192.168.0.250";
    QIcon bulbOn = QIcon("./icons/light_on.png");
    QIcon bulbOff = QIcon("./icons/light_off.png");

    QTimer* resetTimer;

    QVector<Device> inputDevices;
    QVector<Device> outputDevices;

private:
    Ui::MainWindow *ui;

    QNetworkAccessManager *manager;
    QNetworkRequest getRequest;
    QNetworkRequest setRequest;

    void updateList();
    void updateIcons();
    void parseJsonToList(std::string jsonStr);

    bool updateInputs = true;
    bool updateOutputs = true;

private slots:
    void managerFinished(QNetworkReply *reply);

    void toggleLight();
    void resetLight();

    void moveDown();
    void moveUp();

    void keyPressEvent(QKeyEvent *event);
};
#endif // MAINWINDOW_H
