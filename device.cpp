#include "device.h"

Device::Device(){}

Device::Device(std::string _name, std::string _uuid, std::string _index, std::string _type, std::string _dpt, int _minValue, int _maxValue, int _value ) {
    name = _name;
    uuid = _uuid;
    index = _index;
    type = _type;
    dpt = _dpt;
    minValue = _minValue;
    maxValue = _maxValue;
    value = _value;
}
