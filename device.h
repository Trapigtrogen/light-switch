#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>

class Device
{
public:
    Device();
    Device(std::string name, std::string uuid, std::string index, std::string type, std::string dpt, int minValue, int maxValue, int value );

    std::string name;
    std::string uuid;
    std::string index;
    std::string type;
    std::string dpt;
    int minValue;
    int maxValue;
    int value;
};

#endif // DEVICE_H
