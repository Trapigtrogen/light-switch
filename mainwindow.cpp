#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    resetTimer = new QTimer;
    resetTimer->setSingleShot(true);
    connect(resetTimer, SIGNAL(timeout()), this, SLOT(resetLight()));

    manager = new QNetworkAccessManager();
    QObject::connect(manager, SIGNAL( finished(QNetworkReply*)), this, SLOT(managerFinished(QNetworkReply*)) );
    QObject::connect( ui->lightList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(toggleLight()) );
    QObject::connect( ui->LightToggle, SIGNAL(clicked()), this, SLOT(toggleLight()) );

    QObject::connect( ui->DownButton, SIGNAL(clicked()), this, SLOT(moveDown()) );
    QObject::connect( ui->UpButton, SIGNAL(clicked()), this, SLOT(moveUp()) );

    updateList();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::managerFinished(QNetworkReply* reply) {
    // Check reply state
    if (reply->error()) {
        qDebug() << reply->errorString();
        QMessageBox messageBox;
        messageBox.critical( 0,"Error",reply->errorString() );
        messageBox.setFixedSize(500,200);
        return;
    }

    // ANSWER STRING
    std::string answer = reply->readAll().toStdString();
    // Skip if no message
    if(answer[0] == '\0') { return; }

    // Parse json
    if( answer[0] == '[') {
        parseJsonToList(answer);
    }
}

void MainWindow::updateList() {
    getRequest.setUrl(QUrl("http://" + IP + "/API/v1/Elements"));
    manager->get(getRequest);
}

void MainWindow::updateIcons() {
    // Save selected entry to restore later
    QListWidgetItem* selectedItem = ui->lightList->currentItem();

    // Go through every input device to find output counterpart
    for (Device dev : inputDevices) {
        for (auto it = outputDevices.begin(); it != outputDevices.end(); ++it) {
            if (it->index == dev.index) {
                // Find entry from ListWidget
                for(int i = 0; i < ui->lightList->count(); i++) {
                    ui->lightList->setCurrentRow(i);
                    if( ui->lightList->currentItem()->data(Qt::UserRole).toString().toStdString() == dev.uuid) {
                        // Set icon based on light's value
                        if(it->value == 1) {
                            ui->lightList->currentItem()->setData( Qt::DecorationRole, bulbOn);
                        }
                        else {
                            ui->lightList->currentItem()->setData( Qt::DecorationRole, bulbOff);
                        }
                    }
                }
            }
        }
    }

    // Restore selection
    ui->lightList->setCurrentItem(selectedItem);
}

void MainWindow::parseJsonToList(std::string str) {
    if(updateInputs) { inputDevices.clear(); }
    if(updateOutputs) { outputDevices.clear(); }

    //qDebug() << str;
    json j = json::parse(str);
    for(auto it = j.begin(); it != j.end(); ++it) {
        // Create devices
        std::string name = it->at("Name");
        std::string id = it->at("Id");
        std::string index = it->at("Index");
        std::string type = it->at("Type");
        std::string dpt = it->at("DPT");
        int min = 0;
        int max = 100;
        int val = it->at("Value");
        Device device(name, id, index, type, dpt, min, max, val);

        // Push device to appropriate vector
        if( type == "WIE" ) {
            if(updateInputs) { inputDevices.push_back(device); }
        }
        else {
            if(updateOutputs) { outputDevices.push_back(device); }
        }
    }


    if(updateInputs) {
        for(Device dev : inputDevices) {
            ui->lightList->addItem(dev.name.c_str());
            ui->lightList->setCurrentRow(ui->lightList->count() - 1);
            ui->lightList->currentItem()->setData( Qt::UserRole, dev.uuid.c_str());
        }
    }
    updateIcons();

    updateInputs = false;
    updateOutputs = false;
}

void MainWindow::toggleLight() {
    // Locks the button until timed out to prevent spam
    if(resetTimer->remainingTime() > 0) { return; }

    QUrl url("http://" + IP + "/API/v1/Elements/" + ui->lightList->currentItem()->data(Qt::UserRole).toString() + "/Value");
    setRequest.setUrl(url);

    // Send the on call
    manager->put(setRequest, "1");

    // Timed reset for the light
    resetTimer->start(1000);
}

// The switch needs to be called off. This doesn't affect the state of the light
void MainWindow::resetLight() {
    // Send the off call
    manager->put(setRequest, "0");

    // Update icons
    updateOutputs = true;
    updateList();
}

void MainWindow::keyPressEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_Return) {
        toggleLight();
    }
}

void MainWindow::moveDown(){
    ui->lightList->setCurrentRow(ui->lightList->currentRow() + 1);
}

void MainWindow::moveUp(){
    ui->lightList->setCurrentRow(ui->lightList->currentRow() - 1);
}

